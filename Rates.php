<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Rates</title>
    <style>
body {
  font-family: Arial, sans-serif;
  margin: 20px;
}

h1 {
  text-align: center;
}

input[type="text"] {
  width: 75%;
  padding: 5px;
  margin-bottom: 10px;
}

button {
  padding: 10px 20px;
  background-color: #4CAF50;
  color: #fff;
  border: none;
  cursor: pointer;
  align-items: center;
}

button:hover {
  background-color: #45a049;
}

h4 {
  margin-top: 20px;
}

span {
  font-weight: bold;
}

    </style>
    <script type="text/javascript">
        function calc() {
            var voltage = parseFloat(document.getElementById('voltage').value);
            var curA = parseFloat(document.getElementById('curA').value);
            var curRate = parseFloat(document.getElementById('curRate').value);

            var hour = 24;

            var power = voltage * curA;
            var kWh = power * hour * 1000;
            var total = kWh * curRate;

            document.getElementById('powerResult').textContent = formatNumber(power.toFixed(5));
            document.getElementById('rateResult').textContent = formatNumber(curRate.toFixed(5));
        }

        // Function to format number in decimal fraction format
        function formatNumber(number) {
            return Number(number).toFixed(5);
        }
    </script>
</head>
<body>
    <h1>Calculate</h1>
    <medium>Voltage</medium><br>
    <input type="text" name="voltage" id="voltage"><br>
    <small>Voltage (V)</small><br>

    <p><small>Current</small></p>
    <input type="text" name="current" id="curA">
    <p><small>Ampere (A)</small></p>
    <br>

    <p><small><b>CURRENT RATE</b></small></p>
    <input type="text" name="current rate" id="curRate">
    <p><small>sen/kWh</small></p>
    <br>

    <button onclick="calc()">Calculate</button><br>
    <h4>POWER: <span id="powerResult">kW</span></h4>
    <h4>RATE: <span id="rateResult">RM</span></h4>
</body>
</html>

